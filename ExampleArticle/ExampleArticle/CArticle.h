#pragma once

#include <iostream>
#include "CDate.h"

using namespace std; 

class CArticle
{

private:
	static int instantsCount;
	int id;
	int type;
	string label;
	CDate inStockSince;
	double price;

public:
	static const int HARDWARE = 0;
	static const int SOFTWARE = 1;
	CArticle(int id, int type = 0, string lbl = "???", int day = 1, int month = 1, int year = 2000, double price = 0.0);
	CArticle(int id, int type, string lbl, CDate date, double price = 0.0);
	
	bool CArticle::operator<(CArticle& art);
	bool CArticle::operator>(CArticle& art);
	bool CArticle::operator==(CArticle& art);
	CArticle CArticle::operator++(int dummy); 
	CArticle& CArticle::operator++(void); 
	CArticle CArticle::operator--(int dummy);
	CArticle& CArticle::operator--(void);
	friend ostream& operator<< (ostream& out, CArticle& article); 
	friend istream& operator>> (istream& in, CArticle& article); 

	
	static int getInst();
	void setPrice(double price);
	double getPrice();

};

