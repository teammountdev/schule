#pragma once

#include <iostream>
#include "utility.h"


class CDate
{
private:
	int day;
	int month;
	int year;
	static const int SHORT = 1; 
	static const int MEDIUM = 2; 
	static const int FULL = 3; 
public:
	CDate(int day = 1, int month = 1, int year = 2000);
	void setDate(int day, int month, int year);
	bool isLeapYear();
	void show(int format = MEDIUM);
	std::string toString(); 
};

