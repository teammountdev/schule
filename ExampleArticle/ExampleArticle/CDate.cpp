#include "stdafx.h"
#include "CDate.h"
#include "utilityFunctions.cpp"




//Constructor
CDate::CDate(int day, int month, int year) {
	this->day = day;
	this->month = month;
	this->year = year;
}



//setDate
void CDate::setDate(int day, int month, int year) {
	CDate(day, month, year);
}

//isLeapYear
bool CDate::isLeapYear() {
	if ((this->year % 4) && (this->year % 100) && (this->year % 400)) {
		return true;
	}

	return false;
}

//Print Datum
void CDate::show(int format) {
	char *monthFull[12] = { "January", "February", "March", "April", "Mai",
		"June", "Juli", "August", "September", "Oktober", "November", "December" };

	char *monthMedium[12] = { "Jan.", "Feb.", "Mar.", "Apr.", "Mai.",
		"Jun.", "Jul.", "Aug.", "Sep.", "Okt.", "Nov.", "Dec." };

	switch (format)
	{
	case SHORT:
		std::cout << this->day << "." << this->month << "." << this->year << std::endl;
		break;

	case MEDIUM:
		std::cout << this->day << "." << monthMedium[(this->month) - 1] << this->year << std::endl;

		break;

	case FULL:
		std::cout << this->day << "." << monthFull[(this->month) - 1] << " " << this->year << std::endl;
		break;
	default:
		break;
	}
}




std::string CDate::toString() {
	std::string date; 
	return date = TypeToString(this->day) + "-" + TypeToString(this->month) + "-" + TypeToString(this->year); 
}
