#include "stdafx.h"

#include "CDate.h"
#include "CArticle.h"

#include <iostream>
using namespace std;

int main()
{
    CArticle a1(1001, 0, "Optical Mouse", 10, 8, 2012, 12.90);
	string name("NoVirus 2015");
    CDate date(15, 9, 2015);
    CArticle a2(2001, 1, name, date, 39.49);
    CArticle* aPtr = new CArticle(3001);  //Hardware, label="???", price=0.0
    double totalSum;



	cout << a1 << endl; 
	cout << a2 << endl;
	cout << *aPtr << endl;

	aPtr->setPrice(10.0);

	double result = a1.getPrice() + aPtr->getPrice();
	cout << "Total price: " << result << endl;
	cout << "Tank you." << endl;
	//Setze den Preis vom Artikel auf den aPtr verweist auf 10.0
 
	//Berechne den Gesamtpreis aller Artikel

	// Gib den Gesamtpreis aus (printf verboten)
	// Ausgabe:  Total price:  62.39 Euro
	//           Thank you.

	//Gib den Speicher f�r den dynamisch angeforderten Artikel frei.

	CArticle artIn(2000);
	cin >> artIn;
	cout << artIn;
	delete aPtr;
	return 0;
}

