#include "stdafx.h"
#include <iostream>
#include <sstream>

template<typename T>
std::string TypeToString(const T& value)
{
	std::ostringstream oss;
	oss << value;
	return oss.str();
}