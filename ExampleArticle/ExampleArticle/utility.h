#pragma once
#include <iostream>

class utility
{
public:
	template<typename T>
	std::string TypeToString(const T& value)
	{
		std::ostringstream oss;
		oss << value;
		return oss.str();
	}
};

