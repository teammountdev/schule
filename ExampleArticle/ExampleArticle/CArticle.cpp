#include "stdafx.h"
#include "CArticle.h"
int CArticle::instantsCount = 0;

CArticle::CArticle(int id, int type, string lbl, int day, int month, int year, double price) : label(lbl) {
	this->id = id;
	this->type = type;
	this->price = price;
	this->inStockSince = CDate(day, month, year); 
	instantsCount++;
}

CArticle::CArticle(int id, int type, string lbl, CDate date, double price) : label(lbl), inStockSince(date)
{
	this->id = id; 
	this->type = type; 
	this->price = price; 
	instantsCount++; 

}

int CArticle::getInst() {
	return instantsCount;
}

void CArticle::setPrice(double price)
{
	this->price = price;
}

double CArticle::getPrice()
{
	return this->price;
}

bool CArticle::operator<(CArticle& art) {
	if (this->price < art.price) {
		return true;
	}
	return false;
}

bool CArticle::operator>(CArticle& art) {
	if (this->price > art.price) {
		return true;
	}
	return false;
}

bool CArticle::operator==(CArticle& art) {
	if (this->price == art.price) {
		return true;
	}
	return false;
}


ostream& operator<< (ostream& out, CArticle& article) {
	cout << "ID: " << article.id << endl;
	cout << "Name: "; 
	cout << article.label.c_str();
	
	if (article.type) {
		cout << "Type: Software" << endl;
	}
	else {
		cout << "Type: Hardware" << endl; 
	}
	 
	cout << "in Stock since: "; article.inStockSince.show(); 
	cout << "Price: " << article.price << endl; 

	return out;
}

istream& operator>> (istream& in, CArticle& article) {
	cout << "ID: ";
	in >> article.id;
	cout << "Type: ";
	in >> article.type;
	cout << "Label: ";
	char label[50];
	in >> label;
	article.label = label;
	
	int day;
	int month;
	int year;
	cout << "Date day: ";
	in >> day;
	cout << "Date month: ";
	in >> month;
	cout << "Date year: ";
	in >> year;
	article.inStockSince.setDate(day, month, year);

	cout << "Price: ";
	in >> article.price;
	return in;
}

CArticle CArticle::operator++(int dummy) {
	CArticle temp(*this); 
	this->price += 1; 
	return temp; 
}

CArticle& CArticle::operator++() {
	this->price += 1; 
	return *this;
}

CArticle CArticle::operator--(int dummy) {
	CArticle temp(*this); 
	this->price -= 1;
	return temp;
}

CArticle& CArticle::operator--() {
	this->price -= 1;
	return *this;
}