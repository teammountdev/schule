#pragma once
#include <string>
#include <iostream>

using namespace std;

class Instrument
{
protected:
	string name;
	int numberOfMajor;
	string* majorList;
public:
	Instrument& operator=(const Instrument& a);
	Instrument(); 
	Instrument(const Instrument& a);
	Instrument(string name, int numberOfMajor, string* majorList);
	~Instrument();
	void print();
};

