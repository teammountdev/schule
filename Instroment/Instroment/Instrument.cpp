#include "stdafx.h"
#include "Instrument.h"

Instrument::Instrument() : name(""), numberOfMajor(0), majorList(nullptr)
{
}

Instrument::Instrument(string name, int numberOfMajor, string* majorList) : name(name), numberOfMajor(numberOfMajor)
{
	this->majorList = new string[numberOfMajor];
	
	for (int i = 0; i < numberOfMajor; i++) {
		this->majorList[i] = majorList[i];
	}
}


void Instrument::print() {
	cout << "Name: " << name << " " << numberOfMajor << endl << endl;
	cout << "Number of Major: " << endl; 
	for (int i = 0; i < numberOfMajor; i++) {
		cout << majorList[i] << "; ";
	}
}

Instrument::Instrument(const Instrument& a) {
	if (this != &a) {
		this->name = a.name;
		delete[] this->majorList;
		this->majorList = new string[a.numberOfMajor];
		this->numberOfMajor = a.numberOfMajor;
	
		for (int i = 0; i < a.numberOfMajor; i++) {
			this->majorList[i] = a.majorList[i];
		}
	}
}

Instrument& Instrument::operator=(const Instrument& a)
{
	if (this != &a) {
		this->name = a.name;
		delete[] this->majorList;
		this->majorList = new string[a.numberOfMajor];
		this->numberOfMajor = a.numberOfMajor;

		for (int i = 0; i < a.numberOfMajor; i++) {
			this->majorList[i] = a.majorList[i];
		}
	}
	return *this;
}




Instrument::~Instrument()
{
	delete[] majorList;
}
