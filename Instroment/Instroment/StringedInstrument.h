#pragma once
#include "Instrument.h"
#include <iostream>
using namespace std;

class StringedInstrument : protected Instrument
{
protected:
	int numberOfStrings;
	char* stringList;
public:
	StringedInstrument(const StringedInstrument& a);
	StringedInstrument();
	StringedInstrument& operator=(const StringedInstrument& a);
	StringedInstrument(char* name, int numberOfMajor, string* majorList, int numberOfStrings, char* stringList);
	~StringedInstrument();
	void print();
	void setNumberOfStrings(int newNum);
	void setStringList(char* newStrings);
	void setName(char* newName);
};

