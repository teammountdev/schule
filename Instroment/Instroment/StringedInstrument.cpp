#include "stdafx.h"
#include "StringedInstrument.h"

StringedInstrument::StringedInstrument() :
	Instrument(), numberOfStrings(0), stringList(nullptr)
{
	cout << "tesT" << endl;
}

StringedInstrument::StringedInstrument(char* name, int numberOfMajor, string* majorList,
	int numberOfStrings, char* stringList) :
	Instrument(name, numberOfMajor, majorList),
	numberOfStrings(numberOfStrings), stringList(stringList)
{
	this->stringList = new char[numberOfStrings];
	for (int i = 0; i < numberOfStrings; i++) {
		this->stringList[i] = stringList[i];
	}
}

void StringedInstrument::setName(char* newName)
{
	this->name = newName;
}

void StringedInstrument::print() {
	cout << "Name: " << name << " " << numberOfMajor << endl << endl;
	cout << "Number of Major: " << endl;
	for (int i = 0; i < numberOfMajor; i++) {
		cout << majorList[i] << ",";
	}


	cout << endl << endl << "Number of Strings: " << numberOfStrings << endl;
	for (int i = 0; i < numberOfStrings; i++) {
		cout << stringList[i] << ",";
	}

	cout << endl << endl << "-------------------" << endl; 
}

StringedInstrument::StringedInstrument(const StringedInstrument& a) 
	: Instrument(a){
	if (this != &a) {
		delete[] this->stringList;
		this->numberOfStrings = a.numberOfStrings;
		this->stringList = new char[a.numberOfStrings];
		for (int i = 0; i < a.numberOfStrings; i++) {
			this->stringList[i] = a.stringList[i];
		}
	}
}

StringedInstrument& StringedInstrument::operator=(const StringedInstrument& a)
{
	if (this != &a) {
		this->name = a.name;
		delete[] this->majorList;
		this->majorList = new string[a.numberOfMajor];
		this->numberOfMajor = a.numberOfMajor;

		for (int i = 0; i < a.numberOfMajor; i++) {
			this->majorList[i] = a.majorList[i];
		}

		delete[] this->stringList;
		this->numberOfStrings = a.numberOfStrings;
		this->stringList = new char[a.numberOfStrings];
		for (int i = 0; i < a.numberOfStrings; i++) {
			this->stringList[i] = a.stringList[i];
		}
	}

	return *this;
}

void StringedInstrument::setNumberOfStrings(int newNum)
{
	this->numberOfStrings = newNum; 
}

void StringedInstrument::setStringList(char* newStrings)
{
	delete[] this->stringList;
	this->stringList = new char[this->numberOfStrings];
	for (int i = 0; i < this->numberOfStrings; i++) {
		this->stringList[i] = newStrings[i];
	}
}


StringedInstrument::~StringedInstrument()
{
	delete[] stringList;
}
