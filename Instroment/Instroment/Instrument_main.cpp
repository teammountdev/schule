// "Instroment.cpp": Definiert den Einstiegspunkt für die Konsolenanwendung.
//
#include "stdafx.h"
#include <string>
#include "Instrument.h"
#include "StringedInstrument.h"

using namespace std;

int main()
{
	string majorList[] = { "C", "G","A","B","F#" };
	char stringListGuitar[] = { 'E','A','D','G','B','E' };
	char stringListBass[] = { 'E','A','D','G' };

	StringedInstrument guitar1("Gibson", 5, majorList, 6, stringListGuitar);
	guitar1.print();
	cout << endl;
	//Guitar init Gibson

	StringedInstrument guitar2 = guitar1;
	guitar2.print();
	cout << endl;
	//Guitar (Copy Constructor) 

	guitar2.setNumberOfStrings(4);
	guitar2.setStringList(stringListBass);
	guitar2.print();
	cout << endl;
	//Guitar (Changed NumberOfStrings, stringList)

	StringedInstrument guitar3;
	cout << endl << "guitar3";
	guitar3.print();
	cout << endl;
	//Guitar 3

	guitar3 = guitar1;
	guitar3.setName("Fender");
	guitar3.print();
	cout << endl;
	StringedInstrument git4(guitar3);
	git4.print();

	return 0;
}


